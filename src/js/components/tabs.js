function toggleNavClasses(node, tabIndex) {
    const navNodes = Array.from(node.querySelectorAll("[data-tabs-to-open]"));
    navNodes.forEach(node => node.classList.remove("active"));
    navNodes
        .filter(node => node?.getAttribute("data-tabs-to-open") === tabIndex)
        .forEach(node => node?.classList.add("active"));
}

function toggleContentClasses(node, tabIndex) {
    const contentNodes = Array.from(node.querySelectorAll("[data-tab-index]"));

    contentNodes.forEach(node => node.classList.remove("active"));
    contentNodes
        .filter(node => node?.getAttribute("data-tab-index") === tabIndex)
        .forEach(node => node?.classList.add("active"));
}

function openTab(container, index) {
    toggleNavClasses(container, index);
    toggleContentClasses(container, index);
}

function bindEvents() {
    $(document).on("click", "[data-tabs-to-open]", function () {
        const container = this.closest(".js-init--tabs");
        const index = this?.getAttribute("data-tabs-to-open");

        openTab(container, index);
    });
}

function initNav(node, initialTabIndex) {
    openTab(node, initialTabIndex);
}

function initTab(node) {
    const initialTabIndex = node?.getAttribute("data-initial-tab-index");

    initNav(node, initialTabIndex);
}

export default function tabs() {
    bindEvents();
    document.querySelectorAll(".js-init--tabs").forEach(initTab);
}
