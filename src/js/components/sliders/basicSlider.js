export default function basicSlider() {
    $(".js-init--slider-basic").ionRangeSlider({
        skin: "finbee",
        hide_min_max: true,
        hide_from_to: true,
    });
}
