const breakpoint = "900px";

export default function creditRecommendHover() {
    const nodes = document.querySelectorAll(".js-init--credit-recommend-hover");

    function onMouseEnter() {
        this.style.height = `${this.clientHeight}px`;

        const contentBlock = this.querySelector(".js-init--credit-recommend-content");
        contentBlock?.classList?.add("active");
    }

    function onMouseLeave() {
        this.style.height = "";

        const contentBlock = this.querySelector(".js-init--credit-recommend-content");
        contentBlock?.classList?.remove("active");
    }

    nodes.forEach(node => {
        function queryListener(query) {
            if (query.matches) {
                node.removeEventListener("mouseenter", onMouseEnter);
                node.removeEventListener("mouseleave", onMouseLeave);
            } else {
                node.addEventListener("mouseenter", onMouseEnter);
                node.addEventListener("mouseleave", onMouseLeave);
            }
        }

        const mediaQuery = window.matchMedia(`screen and (max-width: ${breakpoint})`);
        queryListener(mediaQuery);
        // noinspection JSDeprecatedSymbols
        mediaQuery.addListener(queryListener);
    });
}
