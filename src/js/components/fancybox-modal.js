export default function () {
    $(document).on("click", "a[data-fancybox-modal]", function (event) {
        event.preventDefault();

        $.fancybox.open({
            src: this.getAttribute("href"),
            type: "inline",
            opts: {
                touch: false,
                toolbar: false,
                smallBtn: false,
                autoFocus: false
            }
        });
    });
}
