class BankTabs {
    breakpoint = "1000px";

    constructor(node) {
        this.node = node;
        this.defaultHTML = this.node.innerHTML;

        this.bindEvents();
    };

    bindEvents = () => {
        this.mediaQuery = window.matchMedia(`screen and (max-width: ${this.breakpoint})`);
        // Run once on init
        this.queryListener(this.mediaQuery);

        // Add event listener
        // noinspection JSDeprecatedSymbols
        this.mediaQuery.addListener(this.queryListener);
    };

    queryListener = (query) => {
        if (query.matches) {
            this.initMobile();
        } else {
            this.initDesktop();
        }
    };

    initMobile = () => {
        const navs = this.node.querySelectorAll("[data-tabs-to-open]");

        navs.forEach(nav => {
            const id = nav.getAttribute("data-tabs-to-open");

            this.node.querySelectorAll(`[data-tab-index="${id}"]`)
                .forEach(node => {
                    $(nav).after(node);
                });
        });
    };

    initDesktop = () => {
        this.node.innerHTML = this.defaultHTML;
    };
}


export default function bankTabsAdaptiveness() {
    const tabs = document.querySelectorAll(".bank-tabs.js-init--tabs");
    tabs.forEach(tab => {
        new BankTabs(tab);
    });
}
