function render(isChecked) {
    const $nodes = $(".js-init--have-credit-block");
    if (isChecked) {
        $nodes.fadeIn(200);
    } else {
        $nodes.fadeOut(200);
    }
}

export default function haveCredit() {
    // Careful, that crap don't work with containers or anything else
    // If any of input was checked it will display all the blocks...
    // I should rewrite it ASAP...
    const $input = $(".js-init--have-credit-checkbox input");
    $input.on("change", function () {
        render(this.checked);
    });

    render($input.is(":checked"));
}
