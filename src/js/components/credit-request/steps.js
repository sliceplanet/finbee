function render(container, tabIndex) {
    $(container)
        .find(`[data-tab-index]`)
        .hide()
        .filter(`[data-tab-index="${tabIndex}"]`)
        .show();

    $(container)
        .find(`[data-listen-tab-index]`)
        .removeClass("current active")
        .filter(`[data-listen-tab-index="${tabIndex}"]`)
        .addClass("current")
        .prevAll()
        .addClass("active");
}

export default function steps() {
    const $creditRequest = $(".js-init--credit-request");

    $creditRequest.find(".js-init--credit-request-goto-step-2").on("click", function () {
        render($creditRequest, 1);
    });

    $creditRequest.find(".js-init--credit-request-goto-step-3").on("click", function () {
        render($creditRequest, 2);
    });

    render($creditRequest, $creditRequest.data("initial-tab-index"));
}
