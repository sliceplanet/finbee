import haveCredit from "./have-credit";
import steps from "./steps"

export default function () {
    // haveCredit();
    // steps();

    $(".js__got-to-next").click(function() {
        var $this = $(this).closest(".credit-request-body-step"); 

        $this.addClass("hide");

        $this.next().css({
          height: $this.height()
        });

        
        $(this).closest(".credit-request-body").find(".credit-request-step")
          .eq($this.index())
          .addClass("active")
          .removeClass("current")
          .next()
          .addClass("current")
        $(this).closest(".credit-request").find(".credit-request-step")
          .eq($this.index() + 1)
          .addClass("current")

        setTimeout(function() {
            $this.removeClass("active show hide").next().addClass("active");
        }, 500);

        setTimeout(function() {
            $this.next().addClass("show")

            $this
              .next()
              .css({
                height: $this
                  .next()
                  .find(".credit-request-body-step__wrap")
                  .height()
              });
        }, 600);
    });
}
