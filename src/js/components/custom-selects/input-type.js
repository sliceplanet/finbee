class SelectInputType {
    constructor(target) {
        this.select = target;
        this.$select = $(this.select);

        this.initSelect();
    }

    initSelect = () => {
        this.$select.selectpicker();
    }
}

export default function inputType() {
    document.querySelectorAll(".js-init--custom-select-input-type:not(.js-lazy--initialized)")
        .forEach(node => {
            new SelectInputType(node);
        });
}
