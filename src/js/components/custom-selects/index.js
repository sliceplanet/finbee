import common from "./common";
import inputType from "./input-type";

export default function customSelects() {
    common();
    inputType();
}

