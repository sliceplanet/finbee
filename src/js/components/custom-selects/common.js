import LazyLoader from "../../vendors/lazy-loader";

class CommonSelect {
    CLASS_LIST = {
        CONTAINER: ".js-init--custom-select-common-container",
        SLIDER_CONTAINER: ".js-init--custom-select-common-slider-container",
        SLIDER: ".js-init--custom-select-common-slider",
        PLACEHOLDER: ".bs-title-option",
    };

    constructor(target) {
        this.select = target;
        this.$select = $(this.select);
        this.container = target.closest(this.CLASS_LIST.CONTAINER);
        this.sliderInput = this.container?.querySelector(this.CLASS_LIST.SLIDER);
        this.hasPlaceholder = false;
        this.canUseSliderUpdate = true;

        this.bindEvents();
        this.initSelect();
    };

    get sliderInstance() {
        return $(this.sliderInput).data("ionRangeSlider");
    }

    bindEvents = () => {
        this.container.addEventListener("click", () => {
            this.$select.selectpicker("toggle");
        });

        this.$select.on({
            "changed.bs.select": this.onSelectChanged,
            "hide.bs.select": this.onSelectHide,
            "loaded.bs.select": this.onSelectLoaded,
            "show.bs.select": this.onSelectShow,
        });
    };

    onSelectLoaded = () => {
        const hasSelectedItem = this.select.value;
        this.container?.classList.toggle("active", hasSelectedItem);

        this.hasPlaceholder = $(this.select.options).filter(this.CLASS_LIST.PLACEHOLDER).length !== 0;
        this.initSlider();
    };

    onSelectShow = () => {
        this.container?.classList.add("active", "show");
    };

    onSelectHide = () => {
        const hasSelectedItem = this.select.value;

        this.container?.classList.toggle("active", hasSelectedItem);
        this.container?.classList.remove("show");
    };

    onSelectChanged = () => {
        if (this.sliderInput && this.canUseSliderUpdate) {
            this.sliderInstance.update({
                from: this.hasPlaceholder ? this.select.selectedIndex - 1 : this.select.selectedIndex,
            });
        }
    };

    initSelect = () => {
        this.$select.selectpicker();
    };

    setSelect = (value) => {
        switch (typeof value) {
            case "string": {
                this.$select.selectpicker("val", value);
                break;
            }
            case "number": {
                const option = this.select
                    ?.options.item(this.hasPlaceholder ? value + 1 : value)
                    ?.getAttribute("value");

                this.$select.selectpicker("val", option);
                break;
            }

            default: {
                console.warn("Unsupported `value` type. Send `string` or `number`");
            }
        }

        const hasSelectedItem = this.select.value;
        this.container?.classList.toggle("active", hasSelectedItem);
    };

    initSlider = () => {
        if (this.sliderInput) {
            const optionsCount = this.select.options.length;
            const stepsCount = optionsCount - 1;
            const selectedOptionIndex = this.select.selectedIndex;

            $(this.sliderInput).ionRangeSlider({
                skin: "finbee",
                hide_min_max: true,
                hide_from_to: true,
                min: 0,
                max: this.hasPlaceholder ? stepsCount - 1 : stepsCount,
                step: 1,
                from: this.hasPlaceholder ? selectedOptionIndex - 1 : selectedOptionIndex,

                onStart: (instance) => {
                    const $container = instance.slider.closest(this.CLASS_LIST.SLIDER_CONTAINER);
                    $container
                        .on({
                            "mousedown touchstart": () => {
                                this.canUseSliderUpdate = false;
                            },
                            "mouseup touched": () => {
                                this.canUseSliderUpdate = true;
                            }
                        });

                    this.canUseSliderUpdate = true;
                },

                onChange: (instance) => {
                    this.setSelect(instance.from)
                },
            });
        }
    }
}


export default function common() {
    document.querySelectorAll("select.js-init--custom-select-common:not(.js-lazy--initialized)")
        .forEach(node => {
            const observer = new LazyLoader({
                target: node,
                onShow: (shownNode) => new CommonSelect(shownNode)
            });

            observer.observe();
        });
}
