import LazyLoader from "../../vendors/lazy-loader";

class NumberInput {
    CLASS_LIST = {
        CONTAINER: ".js-init--number-input-container",
        SLIDER_CONTAINER: ".js-init--number-input-slider-container",
        SLIDER: ".js-init--number-input-slider",
    };

    constructor(target) {
        this.input = target;
        this.container = this.input.closest(this.CLASS_LIST.CONTAINER);
        this.sliderInput = this.container?.querySelector(this.CLASS_LIST.SLIDER);
        this.sliderIsReady = false;

        this.bindEvents();
        this.initInputClasses();
        this.initSlider();
    };

    get sliderInstance() {
        return $(this.sliderInput).data("ionRangeSlider");
    }

    bindEvents = () => {
        this.container.addEventListener("click", (e) => {
            if ($(e.target).closest('.js-init--custom-select-input-type').length) {
                return
            }
            this.input.focus();
        });

        this.input.addEventListener("focusin", () => {
            this.container?.classList.add("active", "focus");

        });

        this.input.addEventListener("focusout", () => {
            this.container?.classList?.toggle("active", this.input.value);
            this.container?.classList?.remove("focus");
        });

        this.input.addEventListener("keyup", () => {
            if (this.sliderIsReady && this.sliderInput) {
                this.sliderInstance.update({
                    from: this.input.value,
                });
            }
        });

        this.input.addEventListener("change", () => {
            const currentValue = this.input.value;

            if (this.sliderIsReady && this.sliderInstance) {
                const sliderInstance = this.sliderInstance;

                const minValue = sliderInstance.result.min;
                const maxValue = sliderInstance.result.max;

                if (currentValue < minValue) {
                    this.input.value = minValue;
                } else if (currentValue > maxValue) {
                    this.input.value = maxValue;
                } else {
                    this.input.value = currentValue;
                }
            }
        });
    };

    initInputClasses = () => {
        this.container?.classList.toggle("active", this.input.value);
    };

    setInputValue = (value) => {
        this.input.value = value;
        this.input.dispatchEvent(new Event("change"));
    };

    initSlider = () => {
        if (this.sliderInput) {
            var $step = 1;
           if ($(this.sliderInput).is('.js-input-sum')) {
             $step = 50000
           }
            $(this.sliderInput).ionRangeSlider({
                skin: "finbee",
                hide_min_max: true,
                hide_from_to: true,
                step: $step,
                onStart: (instance) => {
                    this.sliderIsReady = true;
                    this.setInputValue(instance.from_pretty);
                    this.container?.classList.add("active");
                },

                onChange: (instance) => {
                    this.setInputValue(instance.from);
                    this.setInputValue(instance.from_pretty);
                    if ($(this.sliderInput).is('.js-input-sum')) {
                        if ( instance.from > 999999) {
                            this.setInputValue((Math.round(instance.from/500000)*500000).toString().replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, "$1" + ' '));
                        } else {
                            this.setInputValue((Math.round(instance.from/50000)*50000).toString().replace(/(\d{1,3}(?=(?:\d\d\d)+(?!\d)))/g, "$1" + ' '));
                        }
                    }
                }
            })

        }
    };
}

export default function numberInput() {
    document.querySelectorAll(".js-init--number-input")
        .forEach(node => {
            const observer = new LazyLoader({
                target: node,
                onShow: (observedNode) => new NumberInput(observedNode),
            });

            observer.observe();
        });
}
