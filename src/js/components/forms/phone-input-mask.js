export default function phoneInputMask() {
    const $input = $("input[name=phone]");

    $input.mask("+7 (999) 999 99 99");
    $input.on("click touchend", function () {
        const value = this.value;
        const placeholderIndex = value.indexOf("_");
        const hasPlaceholder = placeholderIndex !== -1;

        if (hasPlaceholder) {
            setTimeout(() => {
                this.setSelectionRange(placeholderIndex, placeholderIndex)
            }, 50);
        }
    });
}
