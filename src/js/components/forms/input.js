export default function input() {
    Array.from(document.querySelectorAll(".js-init--input")).forEach(input => {
        input
            ?.closest(".js-init--input-container")
            ?.classList
            .toggle("active", input.value || input.type === "date");
    });

    $(document)
        .on("focusin", ".js-init--input", function () {
            this?.closest(".js-init--input-container")?.classList?.add("active", "focus");
        })
        .on("focusout", ".js-init--input", function () {
            const container = this?.closest(".js-init--input-container");
            container?.classList?.toggle("active", this.value || this.type === "date");
            container?.classList?.remove("focus");
        });
}
