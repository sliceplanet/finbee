import input from "./input";
import numberInput from "./number-input";
import phoneInputMask from "./phone-input-mask";

export default function forms() {
    input();
    numberInput();
    phoneInputMask();
}
