export default function () {
    const isMac = !!navigator.platform.match(/(Mac|iPhone|iPod|iPad)/i);
    var isSafari = !!navigator.userAgent.match(/Version\/[\d\.]+.*Safari/);
    var iOS =
      /iPad|iPhone|iPod/.test(navigator.userAgent) && !window.MSStream;

    if (isMac) {
        document.querySelector("html").classList.add("is-mac");
    }


    if (isSafari && iOS) {
        document.querySelector("html").classList.add("is-safari-ios");
        // alert("ios");
    } else if (isSafari) {
        document.querySelector("html").classList.add("is-safari");
        // alert("ios safari");
    }
};
