export default function promoGallery() {
    // TODO: Add Lazy-loading
    $(".js-init--promo-gallery").each(function () {
        const $this = $(this);
        const $dots = $this.closest(".js-init--promo-gallery-container").find(".js-init--promo-gallery-dots");

        $this.slick({
            arrows: false,
            slidesToShow: 1,
            slidesToScroll: 1,
            dots: true,
            appendDots: $dots,
            fade: true,
        });
    });
}
