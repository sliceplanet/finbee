export default function sendToMail() {
    $(document).on("click", ".js--init-send-to-email-close", function (event) {
        event.preventDefault();

        $(this).closest(".send-to-email-container").fadeOut(200, function () {
            $(".action-links").fadeIn(200);
        });
    });

    $(document).on("click", ".js--init-send-to-email-open", function (event) {
        event.preventDefault();
        $(this).closest(".action-links").fadeOut(200, function () {
            $(".send-to-email-container").fadeIn(200);
        });
    })
}
