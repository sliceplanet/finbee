const breakpoint = "600px";

function handleClick(event) {
    $(this).toggleClass("open");
    $(this).siblings(".footer-nav-items").slideToggle(200);
}

function init() {
    $(document).on("click", ".footer-nav-header", handleClick);
}

function destroy() {
    $(document).off("click", ".footer-nav-header");
    $(".footer-nav-header.open").removeClass("open");
    $(".footer-nav-items").attr("style", "");
}

function queryListener(query) {
    if (query.matches) {
        init();
    } else {
        destroy();
    }
}

export default function footerMenu() {
    const mediaQuery = window.matchMedia(`screen and (max-width: ${breakpoint})`);
    queryListener(mediaQuery);
    // noinspection JSDeprecatedSymbols
    mediaQuery.addListener(queryListener);
}
