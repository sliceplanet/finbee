import LazyLoader from "../vendors/lazy-loader";
import customSelects from "./custom-selects";
import numberInput from "./forms/number-input";

export default function all() {
  $(".js__textarea").on("focus", function() {
      $(this).parent().addClass("focus");
      $(this).parent().addClass("active");
  });

  $(".js__textarea").on("blur", function() {
      if($(this).val() == "") {
        $(this)
          .parent()
          .removeClass("focus");
        $(this)
          .parent()
          .removeClass("active");
      } else {
          $(this)
            .parent()
            .removeClass("active");
      }
  });


  // Slidedown
  $(".js__slidedown").click(function() {
    $(this).toggleClass("active");
    $(this).find(".slidedown__item-content").slideToggle(200);
  });

  //Comment answer
  $( ".js__comment-answer" ).click(function() {    
    $(this).text($(this).text() == 'Ответить' ? 'Скрыть' : 'Ответить');
    $(this).parent().toggleClass('active');
    $(this).closest('.comment-item').children('.add-comment').slideToggle();
  });



  // Init listcredit slider
  $(".js-init--listcreditslider").slick({
    arrows: false,
    slidesToShow: 2,
    slidesToScroll: 2,
    dots: true,
    appendDots: ".js-init--listcreditslider-dots",
    responsive: [
      {
        breakpoint: 768,
        settings: {
          // slidesToShow: 1.2
          slidesToScroll: 1,
          variableWidth: true,
          appendDots: ".js-init--listcreditslider-dots--mobile"
        }
      }
    ]
  });

  $(".js__open-info-table").click(function() {
    // $(this).parent().parent().parent().slideToggle("active");
    $(this)
      // .parent()
      // .parent()
      // .parent()
      .closest(".tablemax__item")
      .find(".tablemax-info")
      .slideToggle();
    $(this).toggleClass("active");
  });

  $(".js__tab-sort").click(function() {
    var $this = $(this);
    $this.toggleClass("active");
    
    $this.parent().find("input").on("change", function() {
      var val = $(this).parent().find("label").text();
      $this.removeClass("active").find("span").text(val);
    });
  });

  $(".tablemax-info__column-title").click(function() {
    if($(window).width() <= 768)
    $(this).toggleClass("active");
    // $(this).next(".tablemax-info__column").slideToggle().css("display", "flex");
  });

  $(".header-nav-item").click(function() {
    var $this = $(this);

    if(!$this.hasClass("active")) {
      setTimeout(function() {
        $this.toggleClass("open")
        
          jQuery(function ($) {
            $(document).mouseup(function (e) { // событие клика по веб-документу
              var div = $this.next(".header-nav__submenu"); // тут указываем ID элемента
              if (!div.is(e.target) // если клик был не по нашему блоку
                && div.has(e.target).length === 0) { // и не по его дочерним элементам
                $this.removeClass("open")
              }
            });
          });
      }, 10)

    } else {
      $this.removeClass("open")
    }
  });

  $(window).scroll(function() {
    $(".header-city").removeClass("show-popup");
    
    $(".header-nav-item").each(function() {
      if($(this).hasClass("open")) {
        $(this).removeClass("open");
      }
    });
  })

  $(".submenublock__header").click(function() {
    $(this).toggleClass("active");
    $(this).parent().find(".submenublock__container").slideToggle()
  });


  function addRemoveBodyScroll(flag) {
    // if($(window).width() <= 768) {
    //   if(flag) {
    //     // alert("add block");
        
    //     $("html").addClass("block-scroll");
    //     $("body").addClass("block-scroll");
    //   } else {
    //     // alert("remove block");
    //     $("html").removeClass("block-scroll");
    //     $("body").removeClass("block-scroll");
    //   }
    // }
  }

  // City
  $(".js__open-city-select > span").click(function() {
    // $(".popup").removeClass("active");
    $(this).parent().toggleClass("active");
    $(".popup-city").toggleClass("active");
    $(".header-city").removeClass("show-popup");
    addRemoveBodyScroll(true);
    if($(this).parent().hasClass("active") ) {
      $(".popup").removeClass("active");
      $(".popup-city").addClass("active");
    }
  });
  
  $(".js__open-city-popup").click(function() {
    $(".popup").removeClass("active");
    $(this).parent().toggleClass("active");
    $(".popup-city").toggleClass("active");
    $(".header-city").removeClass("show-popup");
    $(".header-city").removeClass("active");
    addRemoveBodyScroll(true);
  });

  $(".js__close-popup").click(function() {
    $(".js__open-city-select").removeClass("active");
    $(".popup").removeClass("active");
    addRemoveBodyScroll(false);
  });

  // open mob rating
  $(".js-tooltip-link").on('click', function() {
    $(this).closest('.with-tooltip').find(".js-rating").toggleClass("active");
    addRemoveBodyScroll(true);
  });

  // close mob rating
  $(".js__close-rating").on('click', function() {
    $(this).closest(".js-rating").removeClass("active");
    addRemoveBodyScroll(false);
  });

  // close email

  $(".js--init-email-close").on('click', function() {
      var $el = $(this);
      $el.closest(".send-to-email-container").fadeOut(200, function () {
          $el.closest(".send-to-email-container").next(".action-links").fadeIn(200);
      });
      return false
  });
  //Review
  $(".js__open-review-popup").click(function() {
    $(".popup").removeClass("active");
    $(this).parent().toggleClass("active");
    $(".popup-review").toggleClass("active");
    addRemoveBodyScroll(true);
  });

  
  // Search
  $(".js__open-search-select").click(function() {
    $(".popup").removeClass("active");
    $(this).toggleClass("active");
    $(".popup-searchbl").toggleClass("active");
    $(".header-city").removeClass("active");
    addRemoveBodyScroll(true);
  });
  
  // Login
  $(".js__open-login-select").click(function() {
    $(".popup").removeClass("active");
    $(this).toggleClass("active");
    $(".popup-login").toggleClass("active");
    $(".header-city").removeClass("active");
    addRemoveBodyScroll(true);
  });
  
  // Login
  $(".js__open-repas").click(function() {
    $(".popup").removeClass("active");
    $(this).toggleClass("active");
    $(".popup-repas").toggleClass("active");
    $(".header-city").removeClass("active");
    addRemoveBodyScroll(true);
  });
  
  // ExtSel
  $(".js__open-extsel").click(function() {
    $(".popup").removeClass("active");
    $(this).toggleClass("active");
    $(".popup-extsel").toggleClass("active");
    addRemoveBodyScroll(true);
  });

  // querycr
  $(".js__open-querycr").click(function() {
    $(".popup").removeClass("active");
    $(this).toggleClass("active");
    $(".popup-querycr").toggleClass("active");
    addRemoveBodyScroll(true);
  });
  
  // ask-question-mob
  $(".js__open-ask-question-mob").click(function() {
    $(".popup").removeClass("active");
    $(this).toggleClass("active");
    $(".popup-ask-question-mob").toggleClass("active");
    addRemoveBodyScroll(true);
  });


  // credit
  $(".js__open-credit").click(function() {
    // console.log($(this).data("popup-mobile"));
    addRemoveBodyScroll(true);
    if ($(this).data("popup-mobile")) {
      // console.log("popup mobile true");

      if ($(window).width() <= 991) {
        // console.log("width mobile");
        $(".popup").removeClass("active");
        $(this).toggleClass("active");
        $(".popup-credit").toggleClass("active");
      }
    } else {
      // console.log("popup mobile false");
      $(".popup").removeClass("active");
      $(this).toggleClass("active");
      $(".popup-credit").toggleClass("active");
    }
  });

  // creditpay
  $(".js__open-creditpay").click(function() {
    $(".popup").removeClass("active");
    $(this).toggleClass("active");
    $(".popup-creditpay").toggleClass("active");
    addRemoveBodyScroll(true);
  });

  // grafic
  $(".js__open-grafic").click(function() {
    $(".popup").removeClass("active");
    $(this).toggleClass("active");
    $(".popup-grafic").toggleClass("active");
    addRemoveBodyScroll(true);
  });




  $(".js__tab-form").click(function() {
    var data = $(this).data("form-check");
    $("[data-form-tab]").removeClass("active");
    $("[data-form-tab='"+ data +"']").addClass("active");
  });

  // toggleClass
  $(".js__click-toggle > span").click(function() {
    $(this).parent().toggleClass("active");
  });

  $(".js__close-this-submenu").click(function() {
    $(this)
      .closest(".js__click-toggle")
      .removeClass("active");
  });

  // Select Search
  $(".js__select-search-open").click(function() {
    var $this = $(this).parent();
    var nrCheck = 0;
    $this.toggleClass("focus");


    // $(".js__select-search-clear").click(function() {
    //   $this.removeClass("focus")
    // });

    $this.find(".js__selectsearch-input").on("input change focus", function() {
      var val = $(this).val();
      var regex = new RegExp(val, "gmi");
      // console.log(regex.exec(val));
      // console.log("====================");
      // console.log("====================");
      // console.log("====================");
      
      
      $this.find(".js__selectsearch-check .custom-checkbox-container").each(function() {
        console.log($(this));
        regex.exec($(this).find(".custom-checkbox-label").text()) ? $(this).show() : $(this).hide()
      });

    });

    $this.find(".js__selectsearch-check .custom-checkbox-container").on("click", function() {
      nrCheck = 0;
      $this.find(".js__selectsearch-check .custom-checkbox-container input") .each(function() {
        if ($(this).prop("checked") == true) {
          nrCheck++;
        }
      });
      $this.find(".js__select-search-nr").text(nrCheck).parent().parent().addClass("selected");
    });
    
    $this.find(".js__select-search-clear ").click(function() {
      $this.find(".js__selectsearch-check .custom-checkbox-container input").each(function() {
        $(this).prop("checked", false) == true;
      });
      $this.find(".js__select-search-nr").text(0).parent().parent().removeClass("selected");
    });

    jQuery(function($) {
      $(document).mouseup(function(e) {
        // событие клика по веб-документу
        var div = $this; // тут указываем ID элемента
        if (
          !div.is(e.target) && // если клик был не по нашему блоку
          div.has(e.target).length === 0
        ) {
          // и не по его дочерним элементам
          $this.removeClass("focus"); // скрываем его
        }
      });
    });
  }); 


  $(".js__form-reset").click(function() {
    // $(this).closest("form").reset();
    $(this).trigger("reset");
  });

  // datepicker
  if ($('.js-datepicker').length) {
      $('.js-datepicker').datepicker({
              numberOfMonths: 1,
              closeText: 'Закрыть',
              prevText: '',
              currentText: 'Сегодня',
              monthNames: ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь',
                  'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'
              ],
              monthNamesShort: ['Янв', 'Фев', 'Мар', 'Апр', 'Май', 'Июн',
                  'Июл', 'Авг', 'Сен', 'Окт', 'Ноя', 'Дек'
              ],
              dayNames: ['воскресенье', 'понедельник', 'вторник', 'среда', 'четверг', 'пятница', 'суббота'],
              dayNamesShort: ['вск', 'пнд', 'втр', 'срд', 'чтв', 'птн', 'сбт'],
              dayNamesMin: ['Вс', 'Пн', 'Вт', 'Ср', 'Чт', 'Пт', 'Сб'],
              weekHeader: 'Не',
              autoClose: true,
              firstDay: 1,
              dateFormat: 'dd/mm/y'
          })
          .on('change', function() {
              $(this).closest('.input-container').addClass('active');
          });
    };

    // remove calculator line
    $( 'body').on('click', '.js-remove-line', function() {
      $(this).parent().remove();
       return false;
    });

      function generate_random(length) {
          var string = '';
          var possible = '23456789ABCDEFHJKLMNPRTVWXYZabcdefghijklmnopqrstuvwxyz';
          if ( ! length ) {
              length = 5
          }
          for ( var i = 0; i < length; i++ ){
              string += possible.charAt( Math.floor( Math.random() * possible.length ) );
          }
          return string;
        }


    // add calculator line
    $( 'body').on('click', '.js-add-line', function() {

      $('.js-init--custom-select-input-type-new').removeClass('js-init--custom-select-input-type-new')
      var $el = $(this),
          $closest = $(this).closest('.js-line');
      $(this).removeClass('js-add-line').addClass('js-remove-line');

        var html = '<div class="credit-calculator-row credit-calculator-row-2 credit-calculator-row_action js-line js-add-block">'+
            '<div class="custom-select-container custom-select-container--disable-cursor-pointer js-init--number-input-container rate-long js-input-sum">'+
                '<div class="custom-select-placeholder"><span>Сумма</span>'+
                '</div>'+
                '<div class="custom-select-input-container">'+
                    '<input class="input js-init--number-input" type="text" name="test43">'+
                '</div>'+
                '<select class="custom-select-input-type js-init--custom-select-input-type" data-style="custom-select-common-type" name="'+ generate_random(5) +'">'+
                    '<option value="option_0" selected>Тнг</option>'+
                    '<option value="option_1">Руб</option>'+
               '</select>'+
                '<div class="custom-select-slider js-init--number-input-slider-container">'+
                    '<input class="js-init--number-input-slider" type="hidden" data-from="0" data-min="0" data-max="10000">'+
                '</div>'+
            '</div>'+
            '<div class="custom-select-container js-init--custom-select-common-container">'+
                '<div class="custom-select-placeholder"><span>Периодичность</span>'+
                '</div>'+
                '<div class="custom-select-arrow js-init--custom-select-common-trigger">'+
                    '<svg class="custom-select-arrow__icon">'+
                        '<use xlink:href="svg/sprite.svg#svg--arrow"></use>'+
                    '</svg>'+
                '</div>'+
                '<select class="custom-select custom-select--common js-init--custom-select-common js-init--custom-select-input-type-new" data-style="custom-select-common-button" name="'+ generate_random(5) +'" title="Выберите опцию">'+
                    '<option value="option0">Пункт 1</option>'+
                    '<option value="option1">Пункт 2</option>'+
                    '<option value="option2">Пункт 3</option>'+
                '</select>'+
            '</div><i class="action-calculator js-add-line"><svg class="cross-close"><use xlink:href="svg/sprite.svg#svg--cross-close"></use></svg><svg class="add"><use xlink:href="svg/sprite.svg#svg--add"></use></svg></i>'+
        '</div>';
      $closest.removeClass('js-add-block').after(html);
      customSelects()

      //
      numberInput();
    });

    $('.js-show-more').on('click', function() {
      $('.js-list').find('.item-hidden').fadeIn();
      return false;
    })
    $('.js-replace').on('click', function() {
      var $el = $(this), 
          $closest = $el.closest('.js-converter-wrap'),
          $from = $closest.find('.js-from'),
          $to = $closest.find('.js-to'),
          fromLabel = $from.find('.custom-select-placeholder').text(),
          toLabel = $to.find('.custom-select-placeholder').text();
      $closest.toggleClass('reverse');
      $from.find('.custom-select-placeholder').text(toLabel);
      $to.find('.custom-select-placeholder').text(fromLabel);
      if ($closest.is('.reverse')) {
        $from.find('.js-init--number-input').attr({'name': 'to'});
        $to.find('.js-init--number-input').attr({'name': 'from'});
        $from.find('.js-init--custom-select-input-type').attr({'name': 'to_type'});
        $to.find('.js-init--custom-select-input-type').attr({'name': 'from_type'});
        $from.find('.js-init--number-input-slider').attr({'class': 'to'}).removeClass('from');
        $to.find('.js-init--number-input-slider').attr({'name': 'from'}).removeClass('to');
      } else {

        $from.find('.js-init--number-input').attr({'name': 'from'});
        $to.find('.js-init--number-input').attr({'name': 'to'});
        $from.find('.js-init--custom-select-input-type').attr({'name': 'from_type'});
        $to.find('.js-init--custom-select-input-type').attr({'name': 'to_type'});
        $from.find('.js-init--number-input-slider').attr({'class': 'to'}).removeClass('to');
        $to.find('.js-init--number-input-slider').attr({'name': 'from'}).removeClass('from');
      }
      return false;
    })
    
  // Banktab
  // function banktabInit(){
  //   console.log("awdawd");
    
  //   if (window.matchMedia("(max-width: 680px)").matches) {
  //     console.log("Init");
  //     $(".js__banktab-slider").slick();
  //   } else {
  //     console.log("Disable");
  //     $(".js__banktab-slider").slick("unslick");
  //   }
  // }

  // banktabInit();
  // $(window).resize(function() {
  //   banktabInit();
  // });

  $("[data-scroll-to]").click(function() {
    if($(window).width() >= 992) {
      var to = $(this).data("scroll-to");
      // console.log(to);
      // console.log("Offset: " + $(to).offset().top);
      
      window.scroll({
        top: $(to).offset().top,
        behavior: "smooth"
      });
    }
  });





  // Popup select city
  window.addEventListener("load", function() {
    document.querySelector(".header-city").classList.add("show-popup");
  });

  $(".js__close-popup-city-show").click(function() {
    $(this)
      .closest(".header-city")
      .removeClass("show-popup");
  });





  // Validation
  // $.validate({});

  function formHideShowInput(el) {
    var inputOne = el.find(".js__input-one input");
    var inputTwo = el.find(".js__input-two");
    var hidden = el.find(".js__input-block-hidden");

    var flag1 = false;
    var flag2 = false;

    inputOne.on("input", function() {
      if(($(this).val()).length >= 3) {
        flag1 = true;
      } else {
        flag1 = false;
      }

      console.log(flag1, flag2);
      if (flag1 && flag2) {
        hidden.addClass("active");
      } else {
        hidden.removeClass("active");
      }
    });
    
    inputTwo.on("input", function() {
      if ($(this).val().length >= 3) {
        flag2 = true;
      } else {
        flag2 = false;
      }

      console.log(flag1, flag2);
      if (flag1 && flag2) {
        hidden.addClass("active");
      } else {
        hidden.removeClass("active");
      }
    });
  }

  formHideShowInput($(".js__form-block"));


  if ($(".youtube-video").length) {
    const observerYoutube = new LazyLoader({
      target: document.querySelector(".youtube-video"),
      onShow: shownNode => {
        var el = $(shownNode).find("iframe");
        el.attr("src", el.data("src"));
      }
    });
    observerYoutube.observe();
  }

  



  // ==========================
  // ==========================
  class SearchPopup {
    constructor() {
      this.root = document.querySelector(".js__city-search-root");
      this.content = ".js__city-search-content";
      this.input = ".js__city-search-input";
      this.items = ".js__city-search-items";
      this.inputText = "";
      this.inputChange();
    }
    
    inputChange = () => {
      
      let input = document.querySelector(this.input);
      input.addEventListener("input", (e) => {
        this.inputText = input.value;
        this.findItem();
      });
    }

    findItem = (val) => {
      let regex = new RegExp(this.inputText, "gmi");

      document.querySelectorAll(this.items).forEach((el) => {
        let text = el.innerText;
        if (!regex.exec(text)) {
          el.classList.add("hide");
          // el.parentNode.classList.add("hide")
          
        } else {
          el.classList.remove("hide");
          // el.parentNode.classList.remove("hide")
        }
      });
    }
  }

  var formCity  = new SearchPopup();

  // ==========================
  // ==========================




  // ==========================
  // ==========================
  $(".js__open-subcredit").click(function() {
    $(this).toggleClass("active");
    $(this)
      .closest(".tablemax__item")
      .find(".tablemax__subcredit")
      .slideToggle();
  });
  // ==========================
  // ==========================

    $('.tabs li a').click(function () {
        $(this).parents('.tab-wrap').find('.tab-cont').addClass('hide-tab');
        $(this).parent().siblings().removeClass('active');
        var id = $(this).attr('href');
        $(id).removeClass('hide-tab');
        $(this).parent().addClass('active');
        return false;
    });

    // Init answer slider
    $(".js-init--answers").slick({
        arrows: false,
        slidesToShow: 3,
        slidesToScroll: 1,
        dots: true,
        autoplay: true,
        appendDots: ".js-init--answers-dots",
        responsive: [
          {
            breakpoint: 800,
            settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
              //variableWidth: true,
              appendDots: ".js-init--answers-dots--mobile"
            }
          },
          {
            breakpoint: 550,
            settings: {
              slidesToShow: 1,
              slidesToScroll: 1,
              //variableWidth: true,
              appendDots: ".js-init--answers-dots--mobile"
            }
          }
        ]
    });

    // Init articles slider
    if ($(".js-init--articles").length) {
      $(".js-init--articles").slick({
          arrows: false,
          slidesToShow: 4,
          slidesToScroll: 2,
          dots: true,
          appendDots: ".js-init--articles-dots",
          responsive: [
            {
              breakpoint: 900,
              settings: {
                slidesToShow: 3,
                slidesToScroll: 1,
                //variableWidth: true,
                appendDots: ".js-init--articles-dots--mobile"
              }
            },
            {
              breakpoint: 768,
              settings: {
                slidesToShow: 2,
                slidesToScroll: 1,
                //variableWidth: true,
                appendDots: ".js-init--articles-dots--mobile"
              }
            },
            {
              breakpoint: 550,
              settings: {
                slidesToShow: 1,
                slidesToScroll: 1,
                //variableWidth: true,
                appendDots: ".js-init--articles-dots--mobile"
              }
            }
          ]
      });
    }

    //Init map slider
    if ($(".js-init--map").length) {

      var initMapSlider = function()  {
      var containerEl = $(".js-init--map")[0];
      var scrollbarWidth = containerEl.clientWidth - containerEl.offsetWidth ;
        $('.js-init--map').css('margin-right', '');
        if (window.innerWidth < 768) {
          if (!$(".js-init--map").is('.slick-initialized')) {
            $(".js-init--map").slick({
              dots: false,
              infinite: true,
              speed: 300,
              slidesToShow: 1,
              centerMode: true,
              variableWidth: true,
              arrows: false
            });
          } 
        } else {
          if ($(".js-init--map").is('.slick-initialized')) {
            $(".js-init--map").slick('unslick');
          } 
          $('.js-init--map').css('margin-right', scrollbarWidth);
        }
      }
      initMapSlider()
      $(window).resize(function() {
        initMapSlider();
      })
    };

    // Init article slider
    $(".js-init--article-slider").slick({
      arrows: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      appendDots: ".js-init--article-dots"
    });

    $(".js--articles-listing-main__slider").slick({
      arrows: false,
      slidesToShow: 1,
      slidesToScroll: 1,
      dots: true,
      appendDots: ".js-init--articles-lisiting-dots"
    });

    // Init articles-choosing slider
    $(".js-init--articles__choosing").slick({
      arrows: false,
      slidesToShow: 4,
      slidesToScroll: 2,
      dots: true,
      appendDots: ".js-init--articles__choosing-dots",
      responsive: [
        {
          breakpoint: 900,
          settings: {
            slidesToShow: 3,
            slidesToScroll: 1,
            //variableWidth: true,
            appendDots: ".js-init--articles-dots--mobile"
          }
        },
        {
          breakpoint: 768,
          settings: {
            slidesToShow: 2,
            slidesToScroll: 1,
            //variableWidth: true,
            appendDots: ".js-init--articles-dots--mobile"
          }
        },
        {
          breakpoint: 550,
          settings: {
            slidesToShow: 1,
            slidesToScroll: 1,
            //variableWidth: true,
            appendDots: ".js-init--articles-dots--mobile"
          }
        }
      ]
  });

    //rating select
    $(".js-custom-select-rating").click(function() {
        $(this).toggleClass('show');
        return false;
    });
    $(".js-custom-select-rating ul .custom-select-rating__item").click(function() {
        $(this).siblings().removeClass('active');
        $(this).addClass('active');
        var text = $(this).html();
        $(this).parents(".js-custom-select-rating").find('.custom-select-rating__title .custom-select-rating__item').html(text);
        $(this).parents(".js-custom-select-rating").removeClass('show');
        return false;
    });

    $(document).on('touchstart click', function(e) {
        if ($(e.target).parents().filter('.js-custom-select-rating:visible').length != 1) {
            $('.js-custom-select-rating').removeClass('show');
        }
        if (window.innerWidth < 768 && $('.js-rating').is('.active') ) {
          if ($(e.target).closest('.js-rating').length  ||  $(e.target).closest('.js-tooltip-link').length || $(e.target).closest('.js__close-rating').length ) {
            return;
          }
          $('.js-rating').removeClass('active');
        }
    });
    
    // map tabs
    $('.js-map-item').on('click', function() {
      $(this).addClass('active').siblings('.js-map-item').removeClass('active');

    })
    $('.js-map-item').on('click', function() {
      $(this).addClass('active').siblings('.js-map-item').removeClass('active');

    })
    $('.js-switcher-tab input').change(function() {
        if($(this).is(':checked')) {
          $('.js-map-check').removeClass('active-tab')
          $('#'+$(this).attr('id')+'-tab').addClass('active-tab')
        }
    });


    // toogle visiblity mob map contacts 
    $('.js-show-contacts').on('click', function(e) {
      var $el = $(this),
          $lessText = $el.attr('data-text-less'),
          $moreText = $el.attr('data-text-more');
      $el.toggleClass('active');
      if ($el.is('.active')) {
        $el.find('span').text($lessText);
        $el.closest('.map-list__inner').find('.js-hide-mob').slideDown(300)
      } else {
        $el.find('span').text($moreText)
        $el.closest('.map-list__inner').find('.js-hide-mob').slideUp(300,function() {
          $el.closest('.map-list__inner').find('.js-hide-mob').css('dispaly', '')
        })
      }
    })

    if ($('.js-filters').length) {
      var replaceFilters  = function() {
        var $clone = $('.js-filters').html();
        if (window.innerWidth < 993 && $('.js-filters-mob').is(':empty')) {
          $('.js-filters-mob').html($clone)
          $('.js-filters').empty()
        }
        if (window.innerWidth >= 993 && $('.js-filters').is(':empty')) {console.log($clone)
          $('.js-filters').html($('.js-filters-mob').html())
          $('.js-filters-mob').empty()
        }
      } 
      replaceFilters()
      $(window).resize(function() {
        replaceFilters();
      })
    }

}

$(window).on('load', function () {
    setTimeout(function () {
        $("[data-bg]").each(function () {
            $(this).css('background-image', 'url(' + $(this).data("bg") + ')');
        });
        $("img[data-src]").each(function () {
            $(this).attr('src', $(this).data("src"));
        });
    }, 200);
});


//raring
/**
 * jQuery Star Rating plugin
 * Joost van Velzen - http://joost.in
 *
 * v 1.0.3
 *
 * cc - attribution + share alike
 * http://creativecommons.org/licenses/by-sa/4.0/
 */

(function( $ ) {
  $.fn.addRating = function(options) {
    var obj = this;
    var objId = $(this).attr('data-id');
    var settings = $.extend({
      max : 5,
      half : true,
      fieldName : 'rating',
      fieldId : objId,
      icon : ''
    }, options );
    this.settings = settings;

    // create the stars
    for(var i = 1 ; i <= settings.max ; i++)
    {
      var star = $('<i/>').addClass('star-icon').html(this.settings.icon+'').data('rating', i).appendTo(this).click(
        function(){
          obj.setRating($(this).data('rating'));
        }
      ).hover(
        function(e){
          obj.showRating($(this).data('rating'), false);
        }, function(){
          obj.showRating(0,false);
        }
      );
    }
  };

  $.fn.setRating = function(numRating) {
    var obj = this;
    $('#'+obj.settings.fieldId).val(numRating);
    obj.showRating(numRating, true);
  };

  $.fn.showRating = function(numRating, force) {
    var obj = this;
    if($('#'+obj.settings.fieldId).val() == '' || force)
    {
      $(obj).find('i').each(function(){
        var icon = obj.settings.icon+'';
        $(this).removeClass('selected');

        if($(this).data('rating') <= numRating)
        {
          icon = obj.settings.icon;
          $(this).addClass('selected');
        }
        $(this).html(icon);
      })
    }
  }

}( jQuery ));

if ($('#rating').length) {
  $('#rating').addRating();
};

/* viewport width */
function viewport() {
  var e = window,
    a = 'inner';
  if (!('innerWidth' in window)) {
    a = 'client';
    e = document.documentElement || document.body;
  }
  return { width: e[a + 'Width'], height: e[a + 'Height'] }
};
/* viewport width */

var viewport_wid = viewport().width;

if (viewport_wid <= 767) {
  if ($('.js-bank-info__title').length) {
    $(".js-bank-info__title").click(function() {
        $(this).siblings('.bank-info__text').slideToggle();
    });
  };
  if ($('.dynamic-info__schedule').length) {
    $("#dynamic-switcher_radio_0").click(function() {
        $('#dynamics-chart').css('display', 'block');
        $('.dynamic-info__courses').css('display', 'none');
    });
    $("#dynamic-switcher_radio_1").click(function() {
      $('#dynamics-chart').css('display', 'none');
      $('.dynamic-info__courses').css('display', 'block');
  });
  };
  $('.bank-courses').detach().insertAfter($('.bank-products'));
  $('.bank-about__aside').detach().insertAfter($('.bank-contacts'));
  $('.dynamic_best-exchange').detach().insertAfter($('.dynamic-head p'));
  if ($('.bank-about__aside').length) {
    $('.listcredit-slider').detach().insertAfter($('.bank-about__aside'));
  }
  if ($('.js-init--tabs').length){
    $('.js-init--tabs')[0].attributes[1].value = null;
  }
  
};

if ($('.js__contacts-btn-submit').length) {
  $(".js__contacts-btn-submit").click(function() {
      $('.contacts__form-form form').css('display', 'none');
      $('.contacts__form-success').css('display', 'flex');
      $([document.documentElement, document.body]).animate({
        scrollTop: $(".contacts__form-form").offset().top
      }, 500);
  });
};

if ($('.bank-details__currency').length) {
  $('.bank-details__currency').click(function(){
    $('.bank-details__currency-select').toggleClass('show');
  });
  $('.bank-details__currency-select span').click(function(){
    $('.bank-details__currency-select').addClass('hide');
    var currencyText =  $(this).text();
    $('.bank-details__currency p').text(currencyText);
  });
};

if ($('#dynamics-chart').length) {
  document.addEventListener('DOMContentLoaded', function () {
    Highcharts.getJSON('./js/components/data/aapl-c.json', function (data) {

        // Create the chart
        var chart = Highcharts.stockChart('dynamics-chart', {

            chart: {
                height: 351
            },

            title: {
                text: ''
            },



            rangeSelector: {
                selected: 1
            },

            series: [{
                name: 'AAPL Stock Price',
                data: data,
                type: 'area',
                threshold: null,
                tooltip: {
                    valueDecimals: 2
                }
            }],

            responsive: {
                rules: [{
                    condition: {
                        maxWidth: 500
                    },
                    chartOptions: {
                        chart: {
                            height: 300
                        },
                        subtitle: {
                            text: null
                        },
                        navigator: {
                            enabled: false
                        }
                    }
                }]
            }
        });
      });  
  });
}

// deposit head switcher

if($('.deposit__switcher').length){
  $("#deposite_radio_0").click(function() {
    $('.deposit-tab').addClass('hide-tab');
    $('.deposit-tab').eq(0).removeClass('hide-tab');
  });
  $("#deposite_radio_1").click(function() {
    $('.deposit-tab').addClass('hide-tab');
    $('.deposit-tab').eq(1).removeClass('hide-tab');
  });
  $("#deposite_radio_2").click(function() {
    $('.deposit-tab').addClass('hide-tab');
    $('.deposit-tab').eq(2).removeClass('hide-tab');
  });
}

// mortgage calculator switcher

if($('.credit-calculator-tab').length){
  $("#value13_radio_0").click(function() {
    $('.credit-calculator-tab').addClass('hide-tab');
    $('.credit-calculator-tab').eq(0).removeClass('hide-tab');
  });
  $("#value13_radio_1").click(function() {
    $('.credit-calculator-tab').addClass('hide-tab');
    $('.credit-calculator-tab').eq(1).removeClass('hide-tab');
  });
}


