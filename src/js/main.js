import "dom4";

import $ from "jquery/dist/jquery";

window.$ = $;
window.jQuery = $;

import svgPolyfill from "svg4everybody/dist/svg4everybody";
import "retinajs/dist/retina";
import "ion-rangeslider";
import "slick-carousel";

// import "./vendors/slick-animation.min";

import "popper.js";
import "bootstrap/js/dist/util";
import "bootstrap/js/dist/dropdown";
import "bootstrap-select";
import "jquery.maskedinput/src/jquery.maskedinput";
import "@fancyapps/fancybox";
import "simplebar";
import "./components/highstock";
import "./components/data";


// import "jquery-form-validator";

import isMac from "./components/is-mac";
import forms from "./components/forms"
import sliders from "./components/sliders";
import jqueryUi from "./components/jquery-ui.min";
import galleries from "./components/galleries";
import creditRecommendHover from "./components/credit-recommend-hover";
import customSelects from "./components/custom-selects";
import creditRequest from "./components/credit-request";
import tabs from "./components/tabs";
import footerMenu from "./components/footer-menu";
import bankTabsAdaptiveness from "./components/bank-tabs-adaptiveness";
import fancyboxModal from "./components/fancybox-modal";
import sendToMail from "./components/send-to-mail";


import all from "./components/all";

svgPolyfill();

$(document).ready(function () {
    isMac();
    forms();
    sliders();
    galleries();
    creditRecommendHover();
    customSelects();
    creditRequest();
    tabs();
    footerMenu();
    bankTabsAdaptiveness();

    fancyboxModal();
    sendToMail();
    all();
});
